
// express application 
//attach port 


//make express application 
import express, { json } from "express";
import { firstRouter } from "./src/router/firstRouter.js";
import { customerRouter } from "./src/router/customerRouter.js";
import { productRouter } from "./src/router/productRouter.js";
import { reviewRouter } from "./src/router/reviewProduct.js";
import { testRouter } from "./src/router/testRouter.js";
import { secondRouter } from "./src/router/secondRouter.js";
import mongoose from "mongoose";
import connectToMongoDB from "./src/connectToDatabase/connectToMongoDB.js";
import { studentRouter } from "./src/router/studentRouter.js";
import { teacherRouter } from "./src/router/teacherRouter.js";
import { bookRouter } from "./src/router/bookRouter.js";
import { traineeRouter } from "./src/router/trainneRouter.js";
let expressApp = express();
// expressApp.use((req,res,next)=>{
//     console.log("i am middleware 0");
//     next()
// },
// (req,res,next)=>{
//     console.log("i am middleware 1");
//     next()
// })
expressApp.use(json())
let port = 8000
expressApp.use("/",firstRouter)
expressApp.use("/seconds",secondRouter)
expressApp.use("/customer",customerRouter)
expressApp.use("/Product",productRouter)
expressApp.use("/review",reviewRouter)
expressApp.use("/test", testRouter)
expressApp.use("/students",studentRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/trainee",traineeRouter)


//attach port to the above application 
expressApp.listen(port,()=>{
    console.log(`The app is connected to port ${port} successfully`);
});

     // C = create ="Post"
    // R = Read    ="red"
   // U = Update   ="patch"
  // D = Delete    ="delete"



 connectToMongoDB()
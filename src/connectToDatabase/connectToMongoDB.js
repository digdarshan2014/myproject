import mongoose from "mongoose";

 //connect our application to mongodb 
let connectToMongoDB = async()=>{
 try {
    await mongoose.connect("mongodb://0.0.0.0:27017/dw8")
    
    console.log("application is connected successfully to mongo database successfully");
  } catch (error) {
    console.log(("Unable to connect to the database! please try again"));
  }
}

export default connectToMongoDB ;


  

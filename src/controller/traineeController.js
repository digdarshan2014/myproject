
export let createTrainee = async(req,res)=>{
    let data = req.body
    try {
        let result = await Trainee.create(data);
        
        res.json({
            success:true,
            message: "Trainee Created Successfully",
            data:result
        })
        
    } catch (error) {
        res.json({
            success: false,
            message: error.message
        })
    }
        
    
}
export let getAllTrainees = async(req,res)=>{
    try {
        let result = await Trainee.find({})
        res.json({
            success:true,
            message: "Trainee read Successfully",
            data:result
        }) 
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
                    
    }

    export let getSpecificTrainee =async(req,res)=>{
        let id = req.params.id;
        try {
            let result = await Trainee.findById(id);
            res.json({
                success:true,
                message:"trainee read successfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message: error.message
            })
        }
    }
    export let deleteTrainee = async (req, res) => {
        let id = req.params.id;
      
        try {
          let result = await Trainee.findByIdAndDelete(id);
      
          res.json({
            success: true,
            message: "Trainee deleted successfully.",
            data: result,
          });
        } catch (error) {
          res.json({
            success: false,
            message: error.message,
          });
        }
      }
     export let updateTrainee = async(req,res)=>{
        let id = req.params.id;
        let data = req.body
        try {
            let result = await Trainee.findByIdAndUpdate(id,data,{new:true})
            res.json({
                success:true,
                message:"Trainee updated successfully",
                data:result
            })
        } catch (error) {
            res.json({
                success:false,
                message:error.message
            })
        }
      }
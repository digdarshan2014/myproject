import { Router, json, response } from "express";

export let secondRouter = Router()

secondRouter
.route("/")
//  middleware is function which has req,res,next
//for 1 request we need to have one request 
// middleware is divided into two categories based on error 1)- i) error middleware
                                                                // (err,req,res,next)=>{}
//                                                               ii) without middleware  
                                                                // (req,res,next)=>{}
                                                            //   2) based on location
                                                            //      i) route middleware
                                                            //     ii) application middleware  
.post((req,res,next)=>{
console.log("i am middleware 1");
res.json(" middleware 1")
next()
},
(req,res,next)=>{
console.log("i am middleware 2 ");

next() // next() function is used to trigger next middleware 
},
(req,res,next)=>{
console.log("i am middleware 3");
})
.get((req,res,next)=>{
console.log("middleware 0");
next("as")
},
(err,req,res,next)=>{
    console.log("middleware 1 "); 
    next()
},
(req,res,next)=>{
    console.log("middleware 2"); 
},
(err,req,res,next)=>{
    console.log("middleware 3");
    
})
            //url: localhost:8000 method: get

.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

secondRouter
.route("/a")
.post((req,res)=>{
     
    res.json({
        success:true,
        message: "Created",
        
    })
        //url: localhost:8000 method: post
})
.get((req,res)=>{
    res.json("I am get method");             //url: localhost:8000 method: get
})
.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

secondRouter
.route("/a/:b/c/:id1")
.post((req,res)=>{
     console.log(req.params);
    res.json({
        success:true,
        message: "Created",
        message: "####"
    })
        //url: localhost:8000 method: post
})
.get((req,res)=>{
    res.json("I am get method");             //url: localhost:8000 method: get
})
.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

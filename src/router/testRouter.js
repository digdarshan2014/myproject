import { Router } from "express";

export let testRouter  = Router()
testRouter
.route("/")
.post((req,res)=>{
    res.json({success: true , message: "Test is created successfully"})
})
.get((req,res)=>{
    res.json({success: true , message: "Test is updated successfully"})
})
.patch((req,res)=>{
    res.json({success: true , message: "Test is read successfully"})
})
.delete((req,res)=>{
    res.json({success: true  , message: " Test is deleted successfully"})
})

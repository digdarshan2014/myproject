import { Router, json, response } from "express";

export let firstRouter = Router()

firstRouter
.route("/")
.post((req,res)=>{
    // console.log("I am post method");
    // console.log(req.body);
    console.log(req.query);  
    res.json({
        success:true,
        message: "Created",
        data : req.body
    })
        //url: localhost:8000 method: post
})
.get((req,res)=>{
    res.json("I am get method");             //url: localhost:8000 method: get
})
.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

firstRouter
.route("/a")
.post((req,res)=>{
     
    res.json({
        success:true,
        message: "Created",
        
    })
        //url: localhost:8000 method: post
})
.get((req,res)=>{
    res.json("I am get method");             //url: localhost:8000 method: get
})
.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

firstRouter
.route("/a/:b/c/:id1")
.post((req,res)=>{
     console.log(req.params);
    res.json({
        success:true,
        message: "Created",
        message: "####"
    })
        //url: localhost:8000 method: post
})
.get((req,res)=>{
    res.json("I am get method");             //url: localhost:8000 method: get
})
.patch((req,res)=>{
    res.json("I am Patch method");           //url: localhost:8000 method: patch  
})
.delete((req,res)=>{
    res.json("I am Delete Method ");         //url: localhost:8000 method: delete
});

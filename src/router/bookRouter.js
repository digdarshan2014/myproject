import { Router } from "express";

export let bookRouter = Router()

bookRouter
.route("/")
.post((req,res)=>{
        
    res.json({
        success:true,
        message: "Book Created Successfully",
        
    })
})
.get((req,res)=>{
    res.json({
        success:true,
        message: "Book read Successfully",
        
    })             
})
.patch((req,res)=>{
    res.json({
        success:true,
        message: "Book updated Successfully",
        
    })            
})
.delete((req,res)=>{
    res.json("Book deleted Successfully ");         
});
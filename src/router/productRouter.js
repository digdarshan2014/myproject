import { Router } from "express";


export let productRouter = Router()
productRouter
.route("/")
.post((req,res)=>{
    res.json({success:true, message:"product is created successfully"})
})
.get((req,res)=>{
    res.json({success: true , message: "Product is updated successfully"})
})
.patch((req,res)=>{
    res.json({success:true , message:"Product is Read  successfully"})
})
.delete((req,res)=>{
    res.json({success:true , message: "Product is Deleted successfully"})
})

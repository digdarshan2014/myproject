import { Router, json } from "express"

export  let reviewRouter = Router()

reviewRouter
.route("/")
.post((req,res)=>{
    res.json({success: true  , message: "Review is created successfully"})
})
.get((req,res)=>{
    res.json(({success: true , message: "Review is updated Successfully"}))
})
.patch((req,res)=>{
    res.json({success: true , message: "Review is read successfully"})
})
.delete((req,res)=>{
    res.json({success: true , message:"Review is deleted Successfully"})
})

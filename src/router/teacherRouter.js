import { Router, json, response } from "express";

export let teacherRouter = Router()

teacherRouter
.route("/")
.post((req,res)=>{
        
    res.json({
        success:true,
        message: "teacher Created Successfully",
        
    })
})
.get((req,res)=>{
    res.json({
        success:true,
        message: "teacher read Successfully",
        
    })             
})
.patch((req,res)=>{
    res.json({
        success:true,
        message: "teacher updated Successfully",
        
    })            
})
.delete((req,res)=>{
    res.json("teacher deleted Successfully ");         
});
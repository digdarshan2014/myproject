import { Router, json, response } from "express";
import { Student } from "../schema/model.js";
import { createStudent, deleteStudent, getAllStudents, getSpecificStudent, updateStudent } from "../controller/studentController.js";

export let studentRouter = Router()

studentRouter
.route("/")
.post(createStudent)
.get(getAllStudents)
.patch((req,res)=>{
    res.json({
        success:true,
        message: "Student updated Successfully",
        
    })            
})


studentRouter
.route("/:id")
.get(getSpecificStudent)
.delete(deleteStudent)
.patch(updateStudent)
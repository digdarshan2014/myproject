import { Router } from "express";

export let traineeRouter = Router()

traineeRouter
.route("/")
.post((req,res)=>{
        
    res.json({
        success:true,
        message: "Trainee Created Successfully",
        
    })
})
.get((req,res)=>{
    res.json({
        success:true,
        message: "Trainee read Successfully",
        
    })             
})
.patch((req,res)=>{
    res.json({
        success:true,
        message: "Trainee updated Successfully",
        
    })            
})
.delete((req,res)=>{
    res.json("Trainee deleted Successfully ");         
});
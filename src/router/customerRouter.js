import { Router } from "express";

export let customerRouter = Router();
customerRouter
.route("/")
.post((req,res)=>{
    res.json({success: true , message:"Customer Created Successfully"})
})
.get((req,res)=>{
    res.json({success:true , message:"Customer read successfully"})
})
.patch((req,res)=>{
    res.json({success: true , message:"Customer updated Successfully"})

})
.delete((req,res)=>{
    res.json({success: true, message:"Customer deleted Successfully"})
})
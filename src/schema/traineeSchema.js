import { Schema } from "mongoose";

let traineeSchema = Schema({
    name:{
        type:String,
        required: [ true , "name field is required"]
    },
    age:{
        type:Number,
        required:[true , "age field is required"],
    },
    isMarried:{
        type: Boolean,
        required:[false]
    },


})

export default traineeSchema

// model is define array 

import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import bookSchema from "./bookSchema.js";
import traineeSchema from "./traineeSchema.js";
// Singular + First letter should be 
export let Student = model("Students",studentSchema)
export let Teacher = model("Teachers",teacherSchema)
export let Books = model("Books",bookSchema)
let Trainee = model("Trainee",traineeSchema)
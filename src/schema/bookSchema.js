import { Schema } from "mongoose";

let bookSchema = Schema({
    name:{
        type:String,
        required: [ true , "name field is required"]
    },
    author:{
        type:String,
        required:[true , "age field is required"],
    },
    price:{
        type: Number,
        required:[true,"Price field is required"]
    },
    isAvailable:{
        type:Boolean,
        required: [true,"Availability is required"]
    }

})
export default bookSchema